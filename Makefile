up:
	docker-compose up -d
in:
	docker exec -it php7-apache2 /var/www/html
stop:
	docker stop $(docker ps -aq)

