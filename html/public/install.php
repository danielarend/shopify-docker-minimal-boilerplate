<?php

// Set variables for our request
$shop = $_GET['shop'];
$api_key = "xxxxxxxxxxxxxxxxxxxx";
//permissoes da loja
$scopes = "read_orders,write_products, write_script_tags, read_themes, write_themes";
$redirect_uri = "https://meudominio.com/hello/generate_token.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();